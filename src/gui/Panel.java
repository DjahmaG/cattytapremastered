/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import environment.entity.RythmBar;
import game.Game;
import state.LevelSelectState;
import state.PlayState;
import util.ImageUtil;

import java.awt.*;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.image.BufferedImage;
import javax.swing.JPanel;

/**
 *
 * @author Cedric Arickx
 */
public class Panel extends JPanel {


    private transient BufferedImage image;
    private transient Graphics2D g;

    private final static int WIDTH = 1024;
    private final static int HEIGHT = 577;

    public Panel() {
        setPreferredSize(new Dimension(WIDTH, HEIGHT));
        requestFocus();

        image = new BufferedImage(WIDTH, HEIGHT, 1);
        g = (Graphics2D) image.getGraphics();
        g.setBackground(Color.black);
        g.setColor(Color.white);
        g.setStroke(new BasicStroke(4f));
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

/*        String fonts[] = GraphicsEnvironment.getLocalGraphicsEnvironment().getAvailableFontFamilyNames();
        for (int i = 0; i < fonts.length; i++) {
            System.out.println(fonts[i]);
        }
        Font font = new Font("Algerian", Font.BOLD, 34);
        g.setFont(font);*/

        addComponentListener(new ComponentAdapter()
        {
            public void componentResized(ComponentEvent evt) {
                image = new BufferedImage(getWidth(), getHeight(), 1);
                g = (Graphics2D) image.getGraphics();
                if (Game.getGameState().baseBackground != null)
                    Game.getGameState().background = ImageUtil.resize(Game.getGameState().baseBackground, getWidth(), getHeight());
                //Game.getGameState().render(g);

                PlayState.Lines.initLines();
                LevelSelectState.Plane.initPlane();
                RythmBar.initRythmBar();
            }
        });

        repaint();
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawImage(image, 0, 0, null);
    }

    public Graphics2D getG() {
        return g;
    }

}
