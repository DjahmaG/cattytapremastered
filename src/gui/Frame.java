/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import util.KeyInputListener;
import javax.swing.JFrame;
import util.MouseInputListener;

import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

/**
 *
 * @author Cedric Arickx
 */
public class Frame extends JFrame {

    private final Panel panel;

    public Frame() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //setResizable(false);
        setFocusable(true);
        setTitle("CattyTap Remastered");
        setVisible(true);
        
        addKeyListener(new KeyInputListener());
        addMouseListener(new MouseInputListener());

        panel = new Panel();
        add(panel);

        pack();
        setLocationRelativeTo(null);
    }

    public Panel getPanel() {
        return panel;
    }

}
