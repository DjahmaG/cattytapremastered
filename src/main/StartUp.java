/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import game.Game;

/**
 *
 * @author Cedric Arickx & Djah'ma Gellinck
 */
public class StartUp {

    public static void main(String[] args) {
        Game.init();
        Game.start();
    }

}
