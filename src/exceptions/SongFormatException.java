package exceptions;

public class SongFormatException extends RuntimeException {

    public SongFormatException(String message) {
        super(message);
    }
}
