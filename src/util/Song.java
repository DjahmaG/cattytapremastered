package util;

import environment.entity.BlueNote;
import environment.entity.InvisibleNote;
import environment.entity.Note;
import environment.entity.PurpleNote;
import exceptions.SongFormatException;
import exceptions.SongNotFoundException;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class Song {

    public final String title = "";
    public int bpm, barLength;
    public final Map<Integer, List<Note>> song = new TreeMap<>();
    public int currentIndex = 0;

    public boolean finished = false;

    public Song(String path) {
        readCSV(path);
    }

    public final void readCSV(String path) throws SongNotFoundException, SongFormatException {
        int bpmTmp, blTmp;
        Scanner sc;

        try {
            sc = new Scanner(new File(path));
        } catch (FileNotFoundException e) {
            throw new SongNotFoundException("The file (" + path + ") wasn't found.");
        }

        try {
            String[] firstLine = sc.nextLine().split(";");
            bpmTmp = Integer.parseInt(firstLine[1]);
            if (firstLine.length >= 4)
                Note.speed = Integer.parseInt(firstLine[3]);
            blTmp = Integer.parseInt(sc.nextLine().split(";")[1]);
        } catch (NumberFormatException nfe) {
            sc.close();
            throw new SongFormatException("Parsing integers failed in the header of " + path);
        }

        sc.nextLine();
        sc.nextLine();
        sc.nextLine();
        sc.nextLine();

        while (sc.hasNext()) {
            int[] notes;
            try {
                notes = Arrays.stream(sc.nextLine().split(";")).mapToInt(Integer::parseInt).toArray();
            } catch (NumberFormatException nfe) {
                sc.close();
                throw new SongFormatException("Mistake while parsing integers of a note in " + path);
            }

            if (notes.length < 6) {
                sc.close();
                throw new SongFormatException("Wrong number of parameters for the note in " + path);
            }

            double interval = 1000 / (bpmTmp / (60.0) * blTmp / 4.0);

            Note note;
            List<Integer> tones = new ArrayList<>();
            for (int i = 5; i < notes.length; i++) {
                tones.add(notes[i]);
            }
            switch (notes[3]) {
                case 1:
                    note = new Note(notes[2],
                            (int) (notes[4] * interval),
                            tones.stream().mapToInt(i -> i).toArray());
                    break;
                case 2:
                    note = new BlueNote(notes[2],
                            (int) (notes[4] * interval),
                            tones.stream().mapToInt(i -> i).toArray());
                    break;
                case 3:
                    note = new PurpleNote(notes[2],
                            (int) (notes[4] * interval),
                            tones.stream().mapToInt(i -> i).toArray());
                    break;
                case 0:
                    note = new InvisibleNote((int) (notes[4] * interval),
                            tones.stream().mapToInt(i -> i).toArray());
                    break;
                default:
                    sc.close();
                    throw new SongFormatException("Wrong note type in " + path);
            }
            note.instrument = notes[1];
            if (!song.containsKey(notes[0])) {
                List<Note> list = new ArrayList<>();
                list.add(note);
                song.put(notes[0], list);
            } else {
                song.get(notes[0]).add(note);
            }
        }


        bpm = bpmTmp;
        barLength = blTmp;
        sc.close();
    }

    public List<Note> nextNotes() {
        if (currentIndex < song.keySet().stream().max(Integer::compareTo).get()) {
            if (song.containsKey(currentIndex))
                return song.get(currentIndex++);
            else
                currentIndex++;
        } else
            finished = true;
        return new ArrayList<>();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("");
        for (int i = 0; i < song.size(); i++) {
            sb.append(i);
            sb.append(": ");
            song.get(i).stream().forEach(n -> {
                sb.append(n.tone + ", " + n.duration);
            });
            sb.append("\n");
        }
        return sb.toString();
    }
}
