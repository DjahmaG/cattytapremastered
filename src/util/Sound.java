/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.FloatControl;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

/**
 *
 * @author Cedric Arickx
 */
public class Sound {

    public static Clip load(String path) {
        AudioInputStream ais;
        Clip clip = null;
        try {
            ais = AudioSystem.getAudioInputStream(new File(path));
            clip = AudioSystem.getClip();
            clip.open(ais);
        } catch (UnsupportedAudioFileException | IOException | LineUnavailableException e) {
            e.printStackTrace();
        }
        return clip;
    }

    public static Clip play(String path) {
        return play(path, 0);
    }

    public static Clip play(Clip c) {
        if (c.isRunning()) {
            c.stop();
        }
        c.setFramePosition(0);
        while (!c.isRunning()) {
            c.start();
        }
        return c;
    }

    public static Clip play(String path, int i) {
        Clip c = load(path);
        if (c.isRunning()) {
            c.stop();
        }
        c.setFramePosition(i);
        while (!c.isRunning()) {
            c.start();
        }
        return c;
    }

    public static void stop(Clip c) {
        if (c.isRunning()) {
            c.stop();
        }
    }

    public static Clip resume(Clip c) {
        if (!c.isRunning())
            c.start();
        return c;
    }

    public static Clip resumeLoop(Clip c) {
        c.loop(Clip.LOOP_CONTINUOUSLY);
        return c;
    }

    public static Clip loop(String path) {
        return loop(path, 0, 0, load(path).getFrameLength() - 1);
    }

    public static Clip loop(String path, int frame) {
        return loop(path, frame, 0, load(path).getFrameLength() - 1);
    }

    public static Clip loop(String path, int start, int end) {
        return loop(path, 0, start, end);
    }

    public static Clip loop(String path, int frame, int start, int end) {
        Clip c = load(path);
        if (c.isRunning()) {
            c.stop();
        }
        c.setLoopPoints(start, end);
        c.setFramePosition(frame);
        c.loop(Clip.LOOP_CONTINUOUSLY);
        return c;
    }

    public static Clip setPosition(Clip c, int frame) {
        c.setFramePosition(frame);
        return c;
    }

    public static int getFrames(Clip c) {
        return c.getFrameLength();
    }

    public static int getPosition(Clip c) {
        return c.getFramePosition();
    }

    public static void close(Clip c) {
        stop(c);
        c.close();
    }

    public static Clip setVolume(Clip c, float f) {
        FloatControl vol = (FloatControl) c.getControl(FloatControl.Type.MASTER_GAIN);
        vol.setValue(f);
        return c;
    }

    public static boolean isPlaying(Clip c) {
        if (c == null) {
            return false;
        }
        return c.isRunning();
    }

}
