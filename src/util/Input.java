/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import environment.entity.Cat;
import game.Game;
import state.PlayState;

import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * @author Cedric Arickx
 */
public class Input {

    public static final List<Integer> pressedKeys;
    public static final List<Integer> releasedKeys;
    public static final List<Integer> typedKeys;

    public static final List<Integer> mouseClicks;
    public static final List<MouseEvent> mouseClickEvents;
    public static final List<Integer> mousePresses;
    public static final List<Integer> mouseReleases;

    public static boolean mouse1Down = false;
    public static boolean mouse3Down = false;

    public static int mouse1DownTicks = 0;
    public static int mouse3DownTicks = 0;

    static {
        pressedKeys = new CopyOnWriteArrayList<>();
        releasedKeys = new CopyOnWriteArrayList<>();
        typedKeys = new CopyOnWriteArrayList<>();

        mouseClicks = new CopyOnWriteArrayList<>();
        mouseClickEvents = new CopyOnWriteArrayList<>();
        mousePresses = new CopyOnWriteArrayList<>();
        mouseReleases = new CopyOnWriteArrayList<>();
    }

    public static void handleInput() {
        Game.getGameState().handlePressedKeys();
        Game.getGameState().handleReleasedKeys();
        Game.getGameState().handleTypedKeys();

        Game.getGameState().handleMouseClicks();
        Game.getGameState().handleMousePresses();
        Game.getGameState().handleMouseReleases();

        Game.getGameState().handleMouseDownTicks();
    }


    public static void addToPressedKeys(Integer e) {
        pressedKeys.add(e);
    }

    public static void addToReleasedKeys(Integer e) {
        releasedKeys.add(e);
    }

    public static void addToTypedKeys(Integer e) {
        typedKeys.add(e);
    }

    public static void addToMouseClicks(Integer e) {
        mouseClicks.add(e);
    }

    public static void addToMouseClickEvents(MouseEvent e) {
        mouseClickEvents.add(e);
    }

    public static void addToMousePresses(Integer e) {
        mousePresses.add(e);
    }

    public static void addToMouseReleases(Integer e) {
        mouseReleases.add(e);
    }

    public static void clear() {
        pressedKeys.clear();
        releasedKeys.clear();
        typedKeys.clear();
        mouseClicks.clear();
        mousePresses.clear();
        mouseReleases.clear();
        mouseClickEvents.clear();
    }

}
