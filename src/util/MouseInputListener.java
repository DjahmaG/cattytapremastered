/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 *
 * @author Cedric Arickx
 */
public class MouseInputListener implements MouseListener {

    @Override
    public void mouseClicked(MouseEvent e) {
        Input.addToMouseClicks(e.getButton());
        Input.addToMouseClickEvents(e);
    }

    @Override
    public void mousePressed(MouseEvent e) {
        Input.addToMousePresses(e.getButton());
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        Input.addToMouseReleases(e.getButton());
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

}
