/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 *
 * @author Cedric Arickx
 */
public class KeyInputListener implements KeyListener {

    @Override
    public void keyTyped(KeyEvent e) {
        Input.addToTypedKeys(e.getKeyCode());
    }

    @Override
    public void keyPressed(KeyEvent e) {
        Input.addToPressedKeys(e.getKeyCode());
    }

    @Override
    public void keyReleased(KeyEvent e) {
        Input.addToReleasedKeys(e.getKeyCode());
    }
    
}
