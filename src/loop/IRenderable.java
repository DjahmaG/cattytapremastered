/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package loop;

import java.awt.Graphics2D;

/**
 *
 * @author Cedric Arickx
 */
public interface IRenderable {
    
    void render(Graphics2D g);

    int getRenderLayer();
    
}
