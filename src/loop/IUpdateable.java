/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package loop;

/**
 *
 * @author Cedric Arickx
 */
public interface IUpdateable {
    
    void update();

    int getUpdatePriority();
    
}
