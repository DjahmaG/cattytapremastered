/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import environment.entity.Entity;

/**
 *
 * @author Cedric Arickx
 */
public abstract class Controller {

    private final Entity entity;

    public Controller(Entity entity) {
        this.entity = entity;
    }

    public Entity getEntity() {
        return entity;
    }

}
