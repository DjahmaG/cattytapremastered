/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import environment.action.Action;
import environment.entity.Entity;
import game.Game;
import loop.IUpdateable;

import java.util.Collections;
import java.util.Comparator;

/**
 * @author Cedric Arickx
 */
public class ActionController extends Controller {

    public ActionController(Entity entity) {
        super(entity);
    }

    public void addAction(Action u) {
        if (u != null) {
            getEntity().getActions().add(u);
            Collections.sort(getEntity().getActions(), Comparator.comparing(IUpdateable::getUpdatePriority));
        }
    }

    public void removeAction(Action u) {
        if (u != null) {
            getEntity().getActions().remove(u);
        }
    }
}
