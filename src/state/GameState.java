package state;

import environment.entity.Entity;
import game.Game;
import loop.IRenderable;
import loop.IUpdateable;

import javax.sound.sampled.Clip;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public abstract class GameState {

    public static BufferedImage baseBackground;
    public static BufferedImage background;
    public List<IRenderable> renderables = new ArrayList<>();
    public List<IUpdateable> updateables = new ArrayList<>();

    public List<Clip> clips = new ArrayList<>();

    public void update() {
        updateables.forEach(IUpdateable::update);
    }

    public void render(Graphics2D g) {
        g.clearRect(0, 0, Game.getFrame().getWidth(), Game.getFrame().getHeight());
        renderBackground(g);
        renderables.stream().sorted(Comparator.comparingInt(IRenderable::getRenderLayer)).forEach((renderable) -> renderable.render(g));
        Game.getFrame().getPanel().repaint();
    }

    public void renderBackground(Graphics2D g) {
        g.drawImage(background, null, 0, 0);
    }

    public void addEntity(Entity entity, boolean renderable, boolean updateable) {
        if (renderable) renderables.add(entity);
        if (updateable) updateables.add(entity);
    }

    public void addEntity(Entity entity) {
        this.addEntity(entity, true, true);
    }

    public void removeEntity(Entity entity, boolean renderable, boolean updateable) {
        if (renderable) renderables.remove(entity);
        if (updateable) updateables.remove(entity);
    }

    public void removeEntity(Entity entity) {
        this.removeEntity(entity, true, true);
    }

    public void clearEntities() {
        renderables.clear();
        updateables.clear();
    }

    public void handleMouseDownTicks() {
    }

    public void handlePressedKeys() {
    }

    public void handleReleasedKeys() {
    }

    public void handleTypedKeys() {
    }

    public void handleMouseClicks() {
    }

    public void handleMousePresses() {
    }

    public void handleMouseReleases() {
    }

}
