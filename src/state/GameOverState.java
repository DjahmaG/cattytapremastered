package state;

import environment.entity.MenuText;
import game.Game;
import util.Input;

import java.awt.event.KeyEvent;
import java.io.*;
import java.util.Scanner;

public class GameOverState extends GameState {

    public final String title;
    public final int rythm;

    public GameOverState(String title, int rythm) {
        this.title = title.split(":")[0];
        this.rythm = rythm;

        renderables.add(new MenuText(250, 250, "you lose lol lmao score: " + rythm));
        updateHighscore();
    }

    @Override
    public void update() {
        super.update();
    }

    public void handleReleasedKeys() {
        Input.releasedKeys.forEach(n -> {
            switch (n) {
                case KeyEvent.VK_R:
                    Game.setGameState(new LevelSelectState());
                    break;
                default:
                    break;
            }
        });
        Input.releasedKeys.clear();
    }

    public void updateHighscore() {
        try {
            // input the (modified) file content to the StringBuffer "input"
            BufferedReader file = new BufferedReader(new FileReader("resources/highscores.txt"));
            StringBuffer inputBuffer = new StringBuffer();
            String line;
            boolean found = false;

            while ((line = file.readLine()) != null) {
                String[] highscore = line.split(":");
                if (highscore[0].equals(title)) {
                    found = true;
                    if (Integer.valueOf(highscore[1]) <= rythm)
                        line = title + ":" + rythm; // replace the line here
                }
                inputBuffer.append(line);
                inputBuffer.append('\n');
                if (found)
                    break;
            }
            if (!found) {
                inputBuffer.append(title + ":" + rythm);
                inputBuffer.append('\n');
            }
            file.close();

            // write the new string with the replaced line OVER the same file
            FileOutputStream fileOut = new FileOutputStream("resources/highscores.txt");
            fileOut.write(inputBuffer.toString().getBytes());
            fileOut.close();

        } catch (Exception e) {
            System.out.println("Problem reading file.");
        }
    }


}
