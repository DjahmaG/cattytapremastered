package state;

import environment.entity.MenuText;
import game.Game;
import util.Input;
import util.ImageUtil;
import util.Sound;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;

public class MenuState extends GameState {

    static {
        try {
            baseBackground = ImageIO.read(new File("resources/images/background.png"));
            background = ImageUtil.resize(baseBackground, Game.getFrame().getWidth(), Game.getFrame().getHeight());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public MenuState() {
        Game.tick = 0;
        super.clearEntities();
        super.addEntity(new MenuText(300, 250, "Press any key to start"));

        clips.add(Sound.loop("resources/music/bgm.wav"));
    }

    public void handleMouseDownTicks() {
        if (Input.mouse1Down) {
            Input.mouse1DownTicks++;
        }
        if (Input.mouse3Down) {
            Input.mouse3DownTicks++;
        }

        if (Input.mouse1DownTicks >= 100) {
        }

        if (Input.mouse3DownTicks >= 100) {
        }
    }

    public void handlePressedKeys() {
        Input.pressedKeys.clear();
    }

    public void handleReleasedKeys() {
        if (!Input.releasedKeys.isEmpty()) {
            Sound.play("resources/sfx/menu.wav");
            Game.setGameState(new LevelSelectState());
        }
        Input.releasedKeys.clear();
    }

    public void handleTypedKeys() {
        Input.typedKeys.forEach(n -> {
            switch (n) {
                default:
                    break;
            }
        });
        Input.typedKeys.clear();
    }

    public void handleMouseClicks() {
        Input.mouseClicks.forEach(n -> {
            switch (n) {
                default:
                    break;
            }
        });
        Input.mouseClicks.clear();
    }

    public void handleMousePresses() {
        Input.mousePresses.forEach(n -> {
            switch (n) {
                case 1:
                    Input.mouse1Down = true;
                    break;
                case 3:
                    Input.mouse3Down = true;
                    break;
                default:
                    break;
            }
        });
        Input.mousePresses.clear();
    }

    public void handleMouseReleases() {
        Input.mouseReleases.forEach(n -> {
            switch (n) {
                case 1:
                    if (Input.mouse1DownTicks < 100) {
                    } else {
                    }
                    Input.mouse1Down = false;
                    Input.mouse1DownTicks = 0;
                    break;
                case 3:
                    if (Input.mouse3DownTicks < 100) {
                    } else {
                    }
                    Input.mouse3Down = false;
                    Input.mouse3DownTicks = 0;
                    break;
                default:
                    break;
            }
        });
        Input.mouseReleases.clear();
    }
}
