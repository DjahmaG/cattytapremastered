package state;

import environment.entity.LevelButton;
import environment.entity.LoadingCat;
import exceptions.SongFormatException;
import exceptions.SongNotFoundException;
import game.Game;
import loop.IRenderable;
import util.Input;
import util.ImageUtil;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;
import java.util.List;

public class LevelSelectState extends GameState {

    public final static List<Integer> keys = new ArrayList<>();
    public static final String SONG_EXTENSION = ".song";

    private static Map<String, Integer> levels = new HashMap<>();

    static {
        try {
            //Background
            baseBackground = ImageIO.read(new File("resources/images/background.png"));
            background = ImageUtil.resize(baseBackground, Game.getFrame().getWidth(), Game.getFrame().getHeight());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public LevelSelectState() {
        Input.clear();
        Game.tick = 0;
        super.clearEntities();

        readLevels();
        readHighscores();

        renderables.add(new Plane());
        addLevels();
    }

    private void addLevels() {
        Iterator<Map.Entry<String, Integer>> it = levels.entrySet().iterator();
        int xs = Plane.pxi + Plane.pWidthI / 32;
        int ys = Plane.pyi + Plane.pWidthI / 16;
        for (int i=0; i < levels.size(); i++) {
            int x = xs + (i % 4)* (Plane.pWidthI / 4);
            int y = ys + (i/4) * (Plane.pWidthI / 5);
            Map.Entry<String, Integer> pair = it.next();
            renderables.add(new LevelButton(x, y, i + 1, pair.getKey() + ": " + pair.getValue().toString()));
        }
    }

    public void handleMouseClicks() {
        Input.mouseClickEvents.forEach(n -> {
            switch (n.getButton()) {
                case 1:
                    //left mouse click
                    clickLevel(n.getX(), n.getY());
                    break;
                default:
                    break;
            }
        });
        Input.mouseClickEvents.clear();
    }

    public void handleReleasedKeys() {
        Input.releasedKeys.forEach(n -> {
            if (keys.contains(n)) keys.remove(n);
            switch (n) {
                case KeyEvent.VK_ESCAPE:
                    Game.setGameState(new MenuState());
                    break;
                default:
                    break;
            }
        });
        Input.releasedKeys.clear();
    }

    public void clickLevel(int x, int y) {
        Optional<LevelButton> str = renderables.stream().filter(n -> n instanceof LevelButton)
                .map(n -> (LevelButton) n)
                .filter(n -> n.getRect().contains(x, y))
                .findAny();
        if (str.isPresent())
            loadPlayState(str.get().title);
    }

    public void loadPlayState(String title) {
        //TODO Make sure this method can't be called again while the song is loading!
        LoadingCat loadingCat = new LoadingCat();
        addEntity(loadingCat);
        new Thread(() -> {
            try {
                //TODO doe dit weg gast
                try {
                    Thread.sleep(1500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                PlayState p = new PlayState(title);
                removeEntity(loadingCat);
                Game.setGameState(p);
            } catch (SongNotFoundException | SongFormatException songException) {
                songException.printStackTrace();
                removeEntity(loadingCat);
            }
        }).start();
    }

    @Override
    public void renderBackground(Graphics2D g) {
        super.renderBackground(g);
    }

    public static void readLevels() {
        Arrays.stream(Objects.requireNonNull((new File("resources/songs")).list()))
                .filter(f -> f.endsWith(SONG_EXTENSION))
                .map(f -> f.replace(SONG_EXTENSION, ""))
                .forEach(n -> levels.put(n, 0));
    }

    public static void readHighscores() {
        try {
            Scanner scanner = new Scanner(new File("resources/highscores.txt"));
            while (scanner.hasNextLine()) {
                String[] highscore = scanner.nextLine().split(":");
                levels.replace(highscore[0], Integer.valueOf(highscore[1]));
            }
            scanner.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static class Plane implements IRenderable {

        public static int pWidht, pHeight, pWidthI, pHeightI;
        public static int px, py, pxi, pyi;

        public Plane() {
            initPlane();
        }

        public static void initPlane() {
            pWidht = Game.getFrame().getWidth() * 22 / 24;
            pHeight = Game.getFrame().getHeight();
            pWidthI = Game.getFrame().getWidth() * 22 / 24 - Game.getFrame().getHeight() /12;
            pHeightI = Game.getFrame().getHeight();

            px = Game.getFrame().getWidth()/24;
            py = Game.getFrame().getHeight()/16;
            pxi = px + Game.getFrame().getHeight()/24;
            pyi = py + Game.getFrame().getHeight()/24;
        }

        @Override
        public void render(Graphics2D g) {
            g.setColor(new Color(242, 231, 203));
            g.fillRoundRect(px, py, pWidht, pHeight, 50 , 50);
            g.setColor(new Color(249, 244, 224));
            g.fillRoundRect(pxi, pyi, pWidthI, pHeightI, 50 , 50);
            g.setColor(Color.BLACK);
            g.drawRoundRect(px, py, pWidht, pHeight, 50 , 50);
            g.setColor(Color.LIGHT_GRAY);
            g.drawRoundRect(pxi, pyi, pWidthI, pHeightI, 50 , 50);
        }

        @Override
        public int getRenderLayer() {
            return 0;
        }
    }
}
