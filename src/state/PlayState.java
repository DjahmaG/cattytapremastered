package state;

import environment.action.RythmBarChange;
import environment.entity.Cat;
import environment.entity.InvisibleNote;
import environment.entity.Note;
import environment.entity.RythmBar;
import exceptions.SongFormatException;
import exceptions.SongNotFoundException;
import game.Game;
import loop.IRenderable;
import util.Input;
import util.Song;
import util.ImageUtil;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.List;
import java.util.stream.Collectors;

import static state.LevelSelectState.SONG_EXTENSION;

public class PlayState extends GameState {

    public final String title;
    public static String topText = "";
    public static int combo = 0;

    public final static List<Integer> keys = new ArrayList<>();
    public final Song song;
    public long start;
    public int interval;
    public static int leftB1, leftB2, rightB1, rightB2;

    public int rhythm = 100;
    public RythmBar rythmBar;

    static {
        try {
            //Background
            baseBackground = ImageIO.read(new File("resources/images/background.png"));
            background = ImageUtil.resize(baseBackground, Game.getFrame().getWidth(), Game.getFrame().getHeight());

            //notecollision
            leftB1 = Lines.left - Note.width / 2;
            leftB2 = Lines.left2 + Note.width / 2;

            rightB1 = Lines.right - Note.width / 2;
            rightB2 = Lines.right2 + Note.width / 2;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public PlayState(String title) throws SongNotFoundException, SongFormatException {
        Game.tick = 0;

        clearEntities();
        addEntity(new Cat(), true, false);
        renderables.add(new Lines());
        rythmBar = new RythmBar();
        addEntity(rythmBar);

        Note.speed = 4;
        this.title = title;
        this.song = new Song(("resources/songs/" + title.split(":")[0] + SONG_EXTENSION));
        interval = 1000000000 / (this.song.bpm / (60) * this.song.barLength / 4);
        start = System.nanoTime();
    }


    @Override
    public void update() {
        super.update();

        if (rhythm <= 0 || (song.finished && (updateables.stream().noneMatch(n -> n instanceof Note)))) {
            Game.setGameState(new GameOverState(title, rhythm));
        }

        if (interval <= System.nanoTime() - start) {
            song.nextNotes().forEach(this::addEntity);
            start = System.nanoTime();
        }

        List<Note> remove = updateables.stream()
                .filter(n -> n instanceof Note)
                .map(n -> (Note)n)
                .filter(n -> n.x + Note.width < 0 || n.x > Game.getFrame().getWidth())
                .collect(Collectors.toList());
        remove.forEach(n -> {
            removeEntity(n);
            combo = 0;
            rythmBar.getActionController().addAction(new RythmBarChange(rythmBar, -10));
        });

        handleInvisibleNotes();
    }

    private void handleInvisibleNotes() {
        //sla onzichtbare, afspeelbare noten op
        List<InvisibleNote> invisibleNotes = updateables.stream()
                .filter(n -> n instanceof InvisibleNote)                        //filter op onzichtbare noten.
                .map(n -> (InvisibleNote) n)
                .filter(n -> n.getCenterX() <= (Lines.left + Lines.width/2))    //zorg dat ze perfect staan.
                .collect(Collectors.toList());

        //Speel alle onzichtbare noten af.
        invisibleNotes.forEach(n -> ((InvisibleNote) n).hit());
        //verwijder ze
        invisibleNotes.forEach(this::removeEntity);

    }

    public void render(Graphics2D g) {
        super.render(g);
        g.drawString(topText, Game.getFrame().getWidth() / 2, Game.getFrame().getHeight() / 9);
    }

    public void handleMouseDownTicks() {
        if (Input.mouse1Down) {
            Input.mouse1DownTicks++;
        }
        if (Input.mouse3Down) {
            Input.mouse3DownTicks++;
        }

        if (Input.mouse1DownTicks >= 100) {
        }

        if (Input.mouse3DownTicks >= 100) {
        }
    }

    public void handlePressedKeys() {
        Input.pressedKeys.stream().filter(n -> !keys.contains(n)).forEach(n -> {
            keys.add(n);
            switch (n) {
                case KeyEvent.VK_E:
                    Cat.pose = Cat.pose == 2 ? 3 : 1;
                    hitNote(0);
                    break;
                case KeyEvent.VK_F:
                    Cat.pose = Cat.pose == 2 ? 3 : 1;
                    hitNote(2);
                    break;
                case KeyEvent.VK_J:
                    Cat.pose = Cat.pose == 1 ? 3 : 2;
                    hitNote(3);
                    break;
                case KeyEvent.VK_I:
                    Cat.pose = Cat.pose == 1 ? 3 : 2;
                    hitNote(1);
                    break;
                case KeyEvent.VK_SPACE:
                    Game.setGameState(new PlayState(title));
                    break;
                default:
                    break;
            }
        });
        Input.pressedKeys.clear();
    }

    public void hitNote(int position) {
        int left = position % 2 == 0 ? leftB1 : rightB1;
        int right = position % 2 == 0 ? leftB2 : rightB2;
        List<Note> notes = updateables.stream()
                .filter(n -> n instanceof Note)
                .map(n -> (Note) n)
                .filter(n -> n.position == position)
                .filter(n -> (n.getCenterX() > left && n.getCenterX() < right))
                .collect(Collectors.toList());
        notes.forEach(n -> {
            if (!n.hit) {
                n.hit();
            }
        });
        if (notes.isEmpty()) {
            combo = 0;
            rythmBar.getActionController().addAction(new RythmBarChange(rythmBar, -10));
        }
    }

    public void releaseNote(int position) {
        int left = position % 2 == 0 ? leftB1 : rightB1;
        int right = position % 2 == 0 ? leftB2 : rightB2;
        List<Note> notes = updateables.stream()
                .filter(n -> n instanceof Note)
                .map(n -> (Note) n)
                .filter(n -> n.position == position)
                .filter(n -> (n.getCenterX() > left && n.getCenterX() < right))
                .collect(Collectors.toList());
        notes.forEach(n -> {
            if (n.hit) {
                n.release(Math.abs((left + right) / 2 - n.getCenterX()));
                updateables.remove(n);
                renderables.remove(n);
            }
        });
    }

    public void handleReleasedKeys() {
        Input.releasedKeys.forEach(n -> {
            if (keys.contains(n)) keys.remove(n);
            switch (n) {
                case KeyEvent.VK_E:
                    Cat.pose = Cat.pose == 1 && !Input.pressedKeys.contains(KeyEvent.VK_F) ? 0 : 2;
                    releaseNote(0);
                    break;
                case KeyEvent.VK_F:
                    Cat.pose = Cat.pose == 1 && !Input.pressedKeys.contains(KeyEvent.VK_E) ? 0 : 2;
                    releaseNote(2);
                    break;
                case KeyEvent.VK_J:
                    Cat.pose = Cat.pose == 2 && !Input.pressedKeys.contains(KeyEvent.VK_I) ? 0 : 1;
                    releaseNote(3);
                    break;
                case KeyEvent.VK_I:
                    Cat.pose = Cat.pose == 2 && !Input.pressedKeys.contains(KeyEvent.VK_J) ? 0 : 1;
                    releaseNote(1);
                    break;
                case KeyEvent.VK_SPACE:
                    break;
                default:
                    break;
            }
        });
        Input.releasedKeys.clear();
    }

    public void handleTypedKeys() {
        Input.typedKeys.forEach(n -> {
            switch (n) {
                case KeyEvent.VK_E:
                    break;
                default:
                    break;
            }
        });
        Input.typedKeys.clear();
    }

    public void handleMouseClicks() {
        Input.mouseClicks.forEach(n -> {
            switch (n) {
                default:
                    break;
            }
        });
        Input.mouseClicks.clear();
    }

    public void handleMousePresses() {
        Input.mousePresses.forEach(n -> {
            switch (n) {
                case 1:
                    Input.mouse1Down = true;
                    break;
                case 3:
                    Input.mouse3Down = true;
                    break;
                default:
                    break;
            }
        });
        Input.mousePresses.clear();
    }

    public void handleMouseReleases() {
        Input.mouseReleases.forEach(n -> {
            switch (n) {
                case 1:
                    if (Input.mouse1DownTicks < 100) {
                    } else {
                    }
                    Input.mouse1Down = false;
                    Input.mouse1DownTicks = 0;
                    break;
                case 3:
                    if (Input.mouse3DownTicks < 100) {
                    } else {
                    }
                    Input.mouse3Down = false;
                    Input.mouse3DownTicks = 0;
                    break;
                default:
                    break;
            }
        });
        Input.mouseReleases.clear();
    }

    public static class Lines implements IRenderable {

        public static int line1Y;   //Bovenste lijn
        public static int line2Y;   //Onderste lijn
        public static int lineHeight;

        public static int left;
        public static int right;
        public static int width;

        public static int left2;
        public static int right2;

        public Lines(){
            initLines();
        }

        static {
            initLines();
        }

        public static void initLines() {
            int panelWidth = Game.getFrame().getPanel().getWidth();
            int panelHeight = Game.getFrame().getPanel().getHeight();

            line1Y = 4 * panelHeight / 15;
            line2Y = 11 * panelHeight / 15;
            lineHeight = 10;

            left = panelWidth /15;
            right = 13 * panelWidth /15;
            width = panelWidth /15;

            left2 = left + width;
            right2 = right + width;
        }

        @Override
        public void render(Graphics2D g) {
            //gray lines
            g.setColor(new Color(240, 240, 240, 80));
            g.fillRect(0, line1Y, Game.getFrame().getPanel().getWidth(), lineHeight);
            g.fillRect(0, line2Y, Game.getFrame().getPanel().getWidth(), lineHeight);

            //yellow lines
            g.setColor(new Color(255, 255, 0, 80));
            g.fillRect(left, line1Y, width, lineHeight);
            g.fillRect(left, line2Y, width, lineHeight);

            g.fillRect(right, line1Y, width, lineHeight);
            g.fillRect(right, line2Y, width, lineHeight);

            //g.setColor(Color.darkGray);
            //g.drawString("e", Panel.WIDTH /15, line1Y + lineHeight);
        }

        @Override
        public int getRenderLayer() {
            return 0;
        }
    }
}
