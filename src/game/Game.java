/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

import gui.Frame;
import state.GameState;
import state.MenuState;
import util.Input;
import util.Sound;

import java.util.Random;

/**
 *
 * @author Cedric Arickx
 */
public class Game {

    private static GameState gameState;
    private static Frame frame;

    public static Random random;
    public static boolean debug;

    private static final int UPDATE_RATE = 1000 / 60;
    public static int tick = 0;

    public static void init() {
        frame = new Frame();
        gameState = new MenuState();

        random = new Random();
        debug = false;
    }

    public static void start() {
        long start;
        long elapsed;
        long wait;

        while (true) {
            start = System.nanoTime();

            Input.handleInput();
            gameState.update();
            gameState.render(frame.getPanel().getG());

            elapsed = System.nanoTime() - start;
            wait = UPDATE_RATE - elapsed / 1000000;
            if (wait < 0) {
                wait = UPDATE_RATE;
            }
            try {
                Thread.sleep(wait);
            } catch (InterruptedException ignored) {
            }
        }
    }

    public static Frame getFrame() {
        return frame;
    }

    public static GameState getGameState() {return gameState;}

    public static void setGameState(GameState gameState) {
        Input.clear();
        Game.gameState.clips.forEach(c -> Sound.stop(c));
        Game.gameState = gameState;
    }
}
