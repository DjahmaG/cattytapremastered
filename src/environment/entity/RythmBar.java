package environment.entity;

import game.Game;
import state.PlayState;

import java.awt.*;

public class RythmBar extends Entity {

    public static int maxWidth;

    static {
        initRythmBar();
    }

    public RythmBar() {
        super.updatePriority = 0;
        super.renderLayer = 0;
    }

    public static void initRythmBar() {
        maxWidth = Game.getFrame().getWidth() * 4 / 5;
    }

    @Override
    public void update() {
        super.update();
    }

    @Override
    public void render(Graphics2D g) {
        int width = (int)((double)maxWidth * Math.atan(((PlayState)Game.getGameState()).rhythm / 100.0) / (Math.PI / 2));
        g.setColor(Color.yellow);
        g.fillRoundRect(
                Game.getFrame().getWidth() / 2 - width / 2,
                Game.getFrame().getWidth() / 8,
                width,
                10, 20, 20);
        g.setColor(Color.gray);
        g.drawRoundRect(
                Game.getFrame().getWidth() / 2 - width / 2,
                Game.getFrame().getWidth() / 8,
                width,
                10, 20, 20);
    }
}