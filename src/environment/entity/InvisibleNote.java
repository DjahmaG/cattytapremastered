package environment.entity;

import java.awt.*;

public class InvisibleNote extends Note {

    public InvisibleNote(int duration, int... tone) {
        super(0, duration, tone);
    }

    @Override
    public void render(Graphics2D g) {
        //Do Nothing.
    }

}
