/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package environment.entity;

import controller.ActionController;
import environment.action.Action;
import loop.IRenderable;
import loop.IUpdateable;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 *
 * @author Cedric Arickx
 */
public abstract class Entity implements IUpdateable, IRenderable {

    private final ActionController actionController;
    private final List<Action> actions;

    public int x, y;

    public int updatePriority = 0;
    public int renderLayer = 0;

    public Entity() {
        actionController = new ActionController(this);
        actions = new CopyOnWriteArrayList<>();
    }

    public Entity(int x, int y) {
        this.x = x;
        this.y = y;

        actionController = new ActionController(this);
        actions = new CopyOnWriteArrayList<>();
    }

    @Override
    public void update() {
        actions.forEach(a -> a.update());
    }

    @Override
    public void render(Graphics2D g) {
    }

    @Override
    public int getUpdatePriority() {
        return updatePriority;
    }

    @Override
    public int getRenderLayer() {
        return renderLayer;
    }

    public void destroy() {
    }

    public ActionController getActionController() {
        return actionController;
    }

    public List<Action> getActions() {
        return actions;
    }

}
