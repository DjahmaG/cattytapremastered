package environment.entity;

import game.Game;
import gui.Frame;
import gui.Panel;
import loop.IRenderable;
import state.PlayState;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.swing.*;

public class LevelButton extends Entity {

    public static final BufferedImage button;
    public final int number;
    public final String title;
    public static final Font font = new Font("TimesRoman", Font.PLAIN, 20);
    public static final FontMetrics metrics;

    static {
        BufferedImage tmp;
        FontMetrics fm;
        try {
            tmp = ImageIO.read(new File("resources/images/levelSelect.png"));

            double ratio = (double)tmp.getHeight() / (double)tmp.getWidth();
            int newW = (Game.getFrame().getWidth() / 6);
            int newH = (int) (newW * ratio);

            Image temp = tmp.getScaledInstance(newW, newH, Image.SCALE_SMOOTH);
            BufferedImage dimg = new BufferedImage(newW, newH, BufferedImage.TYPE_INT_ARGB);

            Graphics2D g2d = dimg.createGraphics();
            g2d.drawImage(temp, 0, 0, null);
            g2d.dispose();

            tmp = dimg;

            fm = Game.getFrame().getPanel().getG().getFontMetrics(font);
        } catch (IOException e) {
            tmp = null;
            fm = null;
            e.printStackTrace();
        }
        button = tmp;
        metrics = fm;
    }

    public LevelButton(int x, int y) {
        this(x, y, 0, "");
    }

    public LevelButton(int x, int y, int numb, String title) {
        super(x, y);
        this.number = numb;
        this.title = title;
    }

    public Rectangle2D getRect() {
        return new Rectangle(x, y, button.getWidth(), button.getHeight());
    }

    @Override
    public void render(Graphics2D g) {
        g.drawImage(button, null, x, y);
        g.setColor(Color.BLACK);
        g.setFont(font);
        g.drawString(String.valueOf(number),
                x + (button.getWidth() - metrics.stringWidth(String.valueOf(number)))/2,
                y + button.getHeight() * 5 / 12);
        g.drawString(title,
                x + (button.getWidth()- metrics.stringWidth(String.valueOf(title)))/2,
                (int) (y + button.getHeight() * 1.2));
    }

/*    public void click() {
        Game.setGameState(new PlayState(title));
    }*/

    @Override
    public int getRenderLayer() {
        return 2;
    }
}
