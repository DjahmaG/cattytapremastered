package environment.entity;

import environment.action.NoteFadeIn;
import environment.action.RythmBarChange;
import game.Game;
import state.PlayState;
import util.ImageUtil;

import javax.imageio.ImageIO;
import javax.sound.midi.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class Note extends Entity {

    public BufferedImage image;
    public static BufferedImage whiteImage;

    public static int width;
    public static int height;

    public final boolean direction;
    public final int position;
    public static double speed = 1;
    public final int duration;
    public final int[] tone;
    public static Synthesizer midiSynth;
    public static MidiChannel[] mChannels;

    public int instrument = 0;

    public boolean hit = false;

    static {
        try {
            whiteImage = ImageIO.read(new File("resources/images/note1.png"));

            //image
            double ratio = 0;

            ratio = whiteImage.getTileWidth() / whiteImage.getTileHeight();

            width = (Game.getFrame().getPanel().getWidth() / 12);
            height = (int) (ratio * width);
            whiteImage = ImageUtil.resize(whiteImage, width, height);

            //midi
            midiSynth = MidiSystem.getSynthesizer();
            midiSynth.open();

            Instrument[] instr = midiSynth.getDefaultSoundbank().getInstruments();
            mChannels = midiSynth.getChannels();

            //midiSynth.loadInstrument(instr[2]);//load an instrument
            mChannels[0].programChange(33);
        } catch (IOException | MidiUnavailableException e) {
            e.printStackTrace();
        }
    }

    public Note(int position, int duration, int... tone) {
        image = whiteImage;

        this.position = position;
        switch (position) {
            //Links-boven
            case 0:
                this.y = PlayState.Lines.line1Y + PlayState.Lines.lineHeight/2 - image.getTileHeight()/2;
                this.x = Game.getFrame().getWidth() * 6 / 15 - image.getTileWidth()/2;
                direction = true;
                break;
            //Rechts-boven
            case 1:
                this.y = PlayState.Lines.line1Y + PlayState.Lines.lineHeight/2 - image.getTileHeight()/2;
                this.x = Game.getFrame().getWidth() * 9 / 15 - image.getTileWidth()/2;
                direction = false;
                break;
            //Links-onder
            case 2:
                this.y = PlayState.Lines.line2Y + PlayState.Lines.lineHeight/2 - image.getTileHeight()/2;
                this.x = Game.getFrame().getWidth() * 6 / 15 - image.getTileWidth()/2;
                direction = true;
                break;
            //Rechts-onder
            case 3:
                this.y = PlayState.Lines.line2Y + PlayState.Lines.lineHeight/2 - image.getTileHeight()/2;
                this.x = Game.getFrame().getWidth() * 9 / 15 - image.getTileWidth()/2;
                direction = false;
                break;
            default:
                direction = true;
                break;
        }

        this.tone = tone;
        this.duration = duration;

        getActionController().addAction(new NoteFadeIn(this));
        renderLayer = 1;
    }

    public void hit() {
        hit = true;
        (new Thread(() -> {
            for (int t : tone){
                mChannels[0].noteOn(t, 60);//On channel 0, play note number 60 with velocity 100
            }

            try {
                Thread.sleep(duration);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            for (int t : tone){
                mChannels[0].noteOff(t);//turn of the note
            }
        })).start();
    }

    public void release(double distance) {
        int score = 0;
        if (distance > 2 * (Game.getFrame().getWidth() / 15 / 2 + image.getWidth() / 2) / 3) {
            PlayState.combo = 0;
            PlayState.topText = "GOOD!";
            score = 10;
        }
        else if (distance > 1 * (Game.getFrame().getWidth() / 15 / 2 + image.getWidth() / 2) / 3) {
            PlayState.combo = 0;
            PlayState.topText = "GREAT!";
            score = 15;
        }
        else if (distance < 1 * (Game.getFrame().getWidth() / 15 / 2 + image.getWidth() / 2) / 3) {
            PlayState.combo++;
            if (PlayState.combo > 1)
                PlayState.topText = "PERFECT X " + PlayState.combo + "!";
            else
                PlayState.topText = "PERFECT!";
            score = 25 + (PlayState.combo / 5);
        }

        ((PlayState) Game.getGameState()).rythmBar.getActionController().addAction(new RythmBarChange(((PlayState) Game.getGameState()).rythmBar, score));
    }

    @Override
    public void render(Graphics2D g) {
        g.drawImage(image,null, x, y);
    }

    @Override
    public void update() {
        super.update();
        x += hit ? 0 : (direction ? -speed : speed);
    }

    public int getCenterX() {
        return x + width / 2;
    }

    public int getCenterY() {
        return y + height / 2;
    }

    public String toString() {
        return this.getClass().getSimpleName() + " " + tone + " - " + duration;
    }
}
