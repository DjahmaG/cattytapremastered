package environment.entity;

import game.Game;
import loop.IRenderable;

import java.awt.*;

public class Plane implements IRenderable {

    private int pWidht, pHeight, pWidthI, pHeightI;
    public double pWidhtFr, pHeightFr, pWidthIFr, pHeightIFr;
    public int px, py, pxi, pyi;
    public int alphaValue = 70;

    public Plane() {
        pWidhtFr = 22/24;
        pHeight = 1;
        pWidthIFr = 16/24;
        pHeightIFr = 1;
        initPlane();
    }

    public Plane(double pWidhtFr, double pHeightFr, double pWidthIFr, double pHeightIFr) {
        this.pWidhtFr = pWidhtFr;
        this.pHeightFr = pHeightFr;
        this.pWidthIFr = pWidthIFr;
        this.pHeightIFr = pHeightIFr;
    }

    private void initPlane() {
        pWidht = (int) (Game.getFrame().getWidth() * pWidhtFr);
        pHeight = (int) (Game.getFrame().getHeight() * pHeightFr);
        pWidthI = Game.getFrame().getWidth() * 22 / 24 - Game.getFrame().getHeight() /12;
        pHeightI = (int) (Game.getFrame().getHeight() * pHeightIFr);

        px = (int) (Game.getFrame().getWidth() * (1 - pWidhtFr) / 2);
        py = pHeightFr >= 1 ? Game.getFrame().getHeight()/14 :
                (int) (Game.getFrame().getHeight() * (1 - pHeightFr) / 2);
        pxi = px + Game.getFrame().getHeight()/24;
        pyi = py + Game.getFrame().getHeight()/24;
    }

    @Override
    public void render(Graphics2D g) {
        //background
        g.setColor(new Color(0, 0, 0, alphaValue));
        g.fillRect(0, 0, Game.getFrame().getWidth(), Game.getFrame().getHeight());

        g.setColor(new Color(242, 231, 203));
        g.fillRoundRect(px, py, pWidht, pHeight, 50 , 50);
        g.setColor(new Color(249, 244, 224));
        g.fillRoundRect(pxi, pyi, pWidthI, pHeightI, 50 , 50);
        g.setColor(Color.BLACK);
        g.drawRoundRect(px, py, pWidht, pHeight, 50 , 50);
        g.setColor(Color.LIGHT_GRAY);
        g.drawRoundRect(pxi, pyi, pWidthI, pHeightI, 50 , 50);
    }

    @Override
    public int getRenderLayer() {
        return 0;
    }
}