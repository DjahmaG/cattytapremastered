package environment.entity;

import util.ImageUtil;

import java.awt.image.BufferedImage;

public class BlueNote extends Note {

    public static BufferedImage blueImage;

    static {
        blueImage = ImageUtil.readAndResize("resources/images/note2.png", width, height);
    }

    public BlueNote(int position, int duration, int... tone) {
        super(position, duration, tone);
        image = blueImage;
    }
}
