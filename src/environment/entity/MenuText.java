package environment.entity;

import game.Game;
import state.MenuState;

import java.awt.*;

public class MenuText extends Entity {

    public String text;

    public final Font font = new Font("TimesRoman", Font.PLAIN, 20);
    public final FontMetrics metrics;
    public int stringWidth;

    //legacy
    public MenuText(int x, int y, String text) {
        this(x, y, text, false);
    }

    public MenuText(int x, int y, String text, boolean centeringOn) {
        super(x, y);
        this.text = text;

        metrics = Game.getFrame().getPanel().getG().getFontMetrics();
        if (centeringOn) {
            this.x = x - metrics.stringWidth(text) / 2;
        }

        super.updatePriority = 0;
        super.renderLayer = 0;
    }

    @Override
    public void update() {
    }

    @Override
    public void render(Graphics2D g) {
        g.setFont(font);
        g.drawString(text, x, y);
    }
}
