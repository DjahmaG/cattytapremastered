package environment.entity;

import game.Game;
import state.PlayState;
import util.ImageUtil;

import java.awt.*;
import java.awt.image.BufferedImage;

public class PurpleNote extends Note {

    public static BufferedImage purpleImage;
    public int length;

    static {
        purpleImage = ImageUtil.readAndResize("resources/images/note3.png", width, height);
    }

    public PurpleNote(int position, int duration, int... tone) {
        super(position, duration, tone);

        length = (int) ((double)duration / 1000.0 * speed * 60);
        image = purpleImage;
    }

    public void hit() {
        super.hit();
        hit = true;
    }

    public void release() {
        if (length <= 10)
            ((PlayState) Game.getGameState()).rhythm += 50;
    }

    @Override
    public void update() {
        super.update();
        if (hit) {
            length -= speed;
        }
    }

    @Override
    public void render(Graphics2D g) {
        g.drawImage(image,null, x, y);
        g.setColor(new Color(175, 147,175));
        g.fillRect(position % 2 == 0 ? x : x - length, y + image.getHeight() / 2 + 5, length, 10);
    }
}
