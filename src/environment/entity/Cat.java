package environment.entity;

import game.Game;
import util.ImageUtil;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Cat extends Entity {

    public static int pose = 0;
    public static List<BufferedImage> catPoses = new ArrayList<>();

    static {
        try {
            catPoses.add(ImageIO.read(new File("resources/images/catWithInstr1.png")));
            catPoses.add(ImageIO.read(new File("resources/images/catWithInstr2.png")));
            catPoses.add(ImageIO.read(new File("resources/images/catWithInstr3.png")));
            catPoses.add(ImageIO.read(new File("resources/images/catWithInstr4.png")));

            int newW = (Game.getFrame().getWidth() / 7);
            int newH = (Game.getFrame().getHeight() / 7);
            catPoses.forEach(i -> ImageUtil.resize(i, newW, newH));
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public Cat() {
        super.updatePriority = 0;
        super.renderLayer = 0;
        pose = 0;
    }

    @Override
    public void update() {
    }

    @Override
    public void render(Graphics2D g) {
        g.drawImage(
                catPoses.get(pose),
                null,
                Game.getFrame().getWidth()/2 - catPoses.get(0).getTileWidth()/2,
                Game.getFrame().getHeight()/2 - catPoses.get(0).getTileHeight()/2);
    }
}
