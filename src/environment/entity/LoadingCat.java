package environment.entity;

import game.Game;
import util.ImageUtil;

import java.awt.*;
import java.awt.image.BufferedImage;

public class LoadingCat extends Entity {

    private int counter = 20;
    private int pose = 1;

    private static final BufferedImage pose1;
    private static final BufferedImage pose2;

    static {
        pose1 = ImageUtil.resize(Cat.catPoses.get(1), 100, 100);
        pose2 = ImageUtil.resize(Cat.catPoses.get(2), 100, 100);
    }

    public LoadingCat() {
    }

    @Override
    public void update() {
        if (counter-- == 0) {
            pose = pose == 1 ? 3 : 1;
            counter = 20;
        }
    }

    @Override
    public void render(Graphics2D g) {
        //TODO 200 and 150 are hardcoded values wich need to be changed relative to the size of the screen.
        g.drawImage(
                pose == 1 ? pose1 : pose2,
                null,
                Game.getFrame().getWidth() - 200,
                Game.getFrame().getHeight() - 150);
    }
}
