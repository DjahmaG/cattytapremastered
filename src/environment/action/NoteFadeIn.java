package environment.action;

import environment.entity.Entity;
import environment.entity.Note;
import util.ImageUtil;

import java.awt.image.BufferedImage;

public class NoteFadeIn extends Action {

    public int duration = 10;

    public NoteFadeIn(Entity self) {
        super(self);
    }

    @Override
    public void onCancelled() {

    }

    @Override
    public void update() {
        BufferedImage img = ((Note) getSelf()).image;
        ((Note) getSelf()).image = ImageUtil.resize(img, img.getWidth() + 1, img.getHeight() + 1);

        if (duration-- <= 0)
            getSelf().getActionController().removeAction(this);
    }

    @Override
    public int getUpdatePriority() {
        return 0;
    }
}
