/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package environment.action;

import environment.entity.Entity;
import loop.IUpdateable;

/**
 * @author Cedric Arickx
 */
public abstract class Action implements IUpdateable {

    private final Entity self;
    private final Entity interactor;

    public Action(Entity self, Entity interactor) {
        this.self = self;
        this.interactor = interactor;
        self.getActionController().addAction(this);
    }

    public Action(Entity self) {
        this(self, null);
    }

    public abstract void onCancelled();

    public Entity getSelf() {
        return self;
    }

    public Entity getInteractor() {
        return interactor;
    }

}
