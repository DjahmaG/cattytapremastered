package environment.action;

import environment.entity.Entity;
import game.Game;
import state.PlayState;

public class RythmBarChange extends Action {

    private int amount;

    public RythmBarChange(Entity self, int amount) {
        super(self);
        this.amount = amount;
    }

    @Override
    public void onCancelled() {

    }

    @Override
    public void update() {
        if (amount > 0) {
            ((PlayState) Game.getGameState()).rhythm++;
            amount--;
        }
        if (amount < 0) {
            ((PlayState) Game.getGameState()).rhythm--;
            amount++;
        }
        if (amount == 0)
            getSelf().getActionController().removeAction(this);
    }

    @Override
    public int getUpdatePriority() {
        return 0;
    }
}
